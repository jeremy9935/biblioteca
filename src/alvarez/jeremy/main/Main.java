package alvarez.jeremy.main;

import alvarez.jeremy.bl.Controller;
import alvarez.jeremy.tl.Administrador;
import alvarez.jeremy.tl.Estudiante;
import alvarez.jeremy.tl.Profesor;
import alvarez.jeremy.tl.Persona;
import java.io.*;
import java.time.LocalDate;

public class Main {

    public static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    public static PrintStream out = System.out;
    static Controller gestor = new Controller();

    public static void main(String[] args) throws IOException {
        crearMenu();
    }

    public static void crearMenu() throws IOException {
        boolean noSalir;
        int opcion;
        do {
            mostrarMenu();
            opcion = leerOpcion();
            noSalir = ejecutarAccion(opcion);

        } while (noSalir);

    }

    public static void mostrarMenu() throws IOException {

        out.println("1.    Registrar Estudiante");
        out.println("2.    Listar Estudiante");
        out.println("3.    Registrar Profesor");
        out.println("4.    Listar Profesor");
        out.println("5.    Registrar Administrador");
        out.println("6.    Listar Administrador");
        out.println("7.    Listar Personas");
        out.println("8.    Salir");

        out.println("");
    }// FIN MOSTRAR  MENÚ

    public static int leerOpcion() throws IOException {
        int opcion;

        out.println("Seleccione su opción que desee:\n");
        opcion = Integer.parseInt(in.readLine());
        out.println();
        return opcion;

    }// FIN LEER OPCION

    public static boolean ejecutarAccion(int popcion) throws IOException {
        boolean noSalir = true;
        boolean define;
        switch (popcion) {

            case 1:
                registrarEstudiante();

                break;
            case 2:
                listarEstudiante();
                break;
            case 3:
                registrarProfesor();
                break;
            case 4:
                listarProfesor();
                break;
            case 5:
                registrarAdministrador();
                break;
            case 6:
                listarAdministrador();
                break;
            case 7:
                listarPersona();
                break;
            case 8:
                out.println("Adiós");
                noSalir = false;
                break;

            default:
                out.println("¡OPCIÓN INVÁLIDA!" + "\n"
                        + "Por favor inténtelo nuevamente");
                out.println();
                break;
        }

        return noSalir;

    }// Fin de 

    public static void registrarEstudiante() throws IOException {
        String nombre;
        String apellido;
        String cedula;
        String carrera;
        String numCreditos;

        out.println("Digite su nombre");
        nombre = in.readLine();
        out.println("Digite sus apellidos");
        apellido = in.readLine();
        out.println("Digite su cedula o su carnet");
        cedula = in.readLine();
        out.println("Digite las o la carrera que transcursa");
        carrera = in.readLine();
        out.println("digite la cantidad de creditos");
        numCreditos = in.readLine();

        gestor.procesarEstudiante(nombre, apellido, cedula, carrera, numCreditos);

    }

    public static void listarEstudiante() throws IOException {
        String[] estudiante = gestor.listarEstudiante();

        Estudiante p = new Estudiante();
        out.println(p);

        for (String dato : estudiante) {
            out.println(dato);

        }

    }

    public static void registrarProfesor() throws IOException {
        String nombre;
        String apellido;
        String cedula;
        String contrato;

        LocalDate fechaDeContratacion;

        out.println("Digite su nombre");
        nombre = in.readLine();
        out.println("Digite sus apellidos");
        apellido = in.readLine();
        out.println("Digite su cedula");
        cedula = in.readLine();
        out.println("Digite el contrato estipulado");
        contrato = in.readLine();
        out.println("digite la fecha con formato de yyyy-mm-dd");
        String fecha = in.readLine();
        fechaDeContratacion = LocalDate.parse(fecha);

        gestor.procesarProfesor(nombre, apellido, cedula, fechaDeContratacion);

    }

    public static void listarProfesor() throws IOException {
        String[] profesor = gestor.listarProfesor();

        Profesor p = new Profesor();
        out.println(p);

        for (String dato : profesor) {
            out.println(dato);

        }

    }

    public static void registrarAdministrador() throws IOException {
        String nombre;
        String apellido;
        String cedula;
        char nombramiento;
        String horasSemanales;

        out.println("Digite su nombre");
        nombre = in.readLine();
        out.println("Digite sus apellidos");
        apellido = in.readLine();
        out.println("Digite su cedula ");
        cedula = in.readLine();
        out.println("Digite el nombramiento");
        nombramiento = in.readLine().charAt(0);
        out.println("digite la cantidad de horas semanales");
        horasSemanales = in.readLine();

        gestor.procesarAdministrador(nombre, apellido, cedula, nombramiento, nombramiento);

    }

    public static void listarAdministrador() throws IOException {
        String[] administrador = gestor.listarAdministrador();

        Administrador p = new Administrador();
        out.println(p);

        for (String dato : administrador) {
            out.println(dato);

        }

    }

    public static void listarPersona() throws IOException {
        String[] persona = gestor.listarPersona();

        Persona p = new Persona();
        out.println(p);

        for (String dato : persona) {
            out.println(dato);

        }

    }

    public static void registarPersona() throws IOException {

        String nombre;
        String apellido;
        String cedula;

        out.println("Digite el nombre");
        nombre = in.readLine();
        out.println("Digite El appellido");
        apellido = in.readLine();
        out.println("Digite su cedula o su carnet");
        cedula = in.readLine();

        gestor.procesarPersona(nombre, apellido, cedula);
    }

}
