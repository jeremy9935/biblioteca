
package alvarez.jeremy.cl;

import alvarez.jeremy.tl.Administrador;
import alvarez.jeremy.tl.Estudiante;
import alvarez.jeremy.tl.Profesor;
import alvarez.jeremy.tl.Persona;
import java.util.ArrayList;

public class CapaLogica {
    
    ArrayList<Estudiante> estudiante = new ArrayList<>();
    ArrayList<Profesor> profesor = new ArrayList<>();
    ArrayList<Administrador> administrador = new ArrayList<>();
    ArrayList<Persona> persona = new ArrayList<>();
    
    public void registrarEstudiante(Estudiante obj) {
        //agregar el elemento al array list

        estudiante.add(obj);
    }
    
    public String[] getEstudiante() {
        String[] data = new String[estudiante.size()];
        int posicion = 0;
        //recorrer araylist con for each

        for (Estudiante dato : estudiante) {
            String info = dato.getNombre() + " " + dato.getApellido() + " "
                    + dato.getCedula() + " " + dato.getCarrera() + " "
                    + dato.getNumCreditos();

            data[posicion] = info;
            posicion++;

        }
        return data;
    }
    
     public void registrarProfesor(Profesor obj) {
        //agregar el elemento al array list

        profesor.add(obj);
    }
    
    public String[] getProfesor() {
        String[] data = new String[profesor.size()];
        int posicion = 0;
        //recorrer araylist con for each

        for (Profesor dato : profesor) {
            String info = dato.getNombre() + " " + dato.getApellido() + " "
                    + dato.getCedula() + " " + dato.getTipoContrato() + " "
                    + dato.getFechaDeContratacion();

            data[posicion] = info;
            posicion++;

        }
        return data;
    }
    public void registrarAdministrador(Administrador obj) {
        //agregar el elemento al array list

        administrador.add(obj);
    }
    
    public String[] getAdministrador() {
        String[] data = new String[administrador.size()];
        int posicion = 0;
        //recorrer araylist con for each

        for (Administrador dato : administrador) {
            String info = dato.getNombre() + " " + dato.getApellido() + " "
                    + dato.getCedula() + " " + dato.getNombramiento()+ " "
                    + dato.getHorasSemanales();

            data[posicion] = info;
            posicion++;

        }
        return data;
    }
    
    
    
    public void registarPersona(Persona obj) {
        //agregar el elemento al array list

        persona.add(obj);
    }
    
    public String[] getPersona() {
        String[] data = new String[persona.size()];
        int posicion = 0;
        //recorrer araylist con for each

        for (Persona dato : persona) {
            String info = dato.getNombre() + " " + dato.getApellido() + " "
                    + dato.getCedula() ;

            data[posicion] = info;
            posicion++;

        }
        return data;
    }

    
}
