
package alvarez.jeremy.bl;

import alvarez.jeremy.cl.CapaLogica;
import alvarez.jeremy.tl.Administrador;
import alvarez.jeremy.tl.Estudiante;
import alvarez.jeremy.tl.Persona;
import alvarez.jeremy.tl.Profesor;
import java.time.LocalDate;

public class Controller {

    CapaLogica logica = new CapaLogica();
    
    public Controller() {
        
    }
    
    public void procesarEstudiante(String nombre, String apellido, String cedula, 
            String carrera, String numCreditos) {
        
        Estudiante tmpEstudiante = new Estudiante();
        
        tmpEstudiante.setNombre(nombre);
        tmpEstudiante.setApellido(apellido);
        tmpEstudiante.setCedula(cedula);
        tmpEstudiante.setCarrera(carrera);
        tmpEstudiante.setNumCreditos(numCreditos);
        
        
        logica.registrarEstudiante(tmpEstudiante);
        logica.registarPersona(tmpEstudiante);
        
    }
    
    public String[] listarEstudiante() {
        return logica.getEstudiante();
    }
    
    public void procesarProfesor(String nombre, String apellido, String cedula,LocalDate fechaDeContratacion){
        
        Profesor tmpProfesor = new Profesor();
        
        tmpProfesor.setNombre(nombre);
        tmpProfesor.setApellido(apellido);
        tmpProfesor.setCedula(cedula);
        tmpProfesor.setTipoContrato(cedula);
        tmpProfesor.setFechaDeContratacion(fechaDeContratacion);
        
        
        logica.registrarProfesor(tmpProfesor);
        
        logica.registarPersona(tmpProfesor);
    }
    public String[] listarProfesor() {
        return logica.getProfesor();
    }
    
public void procesarAdministrador(String nombre, String apellido, String cedula,
            char nombramiento, int horasSemanales){
        
        Administrador tmpAdministrador = new Administrador();
        
        tmpAdministrador.setNombre(nombre);
        tmpAdministrador.setApellido(apellido);
        tmpAdministrador.setCedula(cedula);
        tmpAdministrador.setNombramiento(nombramiento);
        
        
        logica.registrarAdministrador(tmpAdministrador);
        logica.registarPersona(tmpAdministrador);
    }
    public String[] listarAdministrador() {
        return logica.getAdministrador();
    }
    
    public void procesarPersona (String nombre, String apellido, String cedula){
        Persona tmpPersona = new Persona();
        
        tmpPersona.setNombre(nombre);
        tmpPersona.setApellido(apellido);
        tmpPersona.setCedula(cedula);
        
        logica.registarPersona(tmpPersona);
        
    }
    
    
    public String[] listarPersona(){
        return logica.getPersona();
    }

}
