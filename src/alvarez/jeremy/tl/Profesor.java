/*
los datos del profesor son los siguiente

fecha de creacion de la clse profesor 17/6/2019
*/
package alvarez.jeremy.tl;

import alvarez.jeremy.tl.Administrador;
import java.time.LocalDate;

public class Profesor extends Persona{
    
    protected String tipoContrato;
    private LocalDate fechaDeContratacion;

    public Profesor() {
        super();
    }

    
    public Profesor(String nombre, String apellido,String cedula,
            char nombramiento, int horasSemanales,String tipoContrato, LocalDate fechaDeContratacion) {
        super(nombre, apellido, cedula);
        this.tipoContrato = tipoContrato;
        this.fechaDeContratacion = fechaDeContratacion;
    }

    public String getTipoContrato() {
        return tipoContrato;
    }

    public void setTipoContrato(String tipoContrato) {
        this.tipoContrato = tipoContrato;
    }

    public LocalDate getFechaDeContratacion() {
        return fechaDeContratacion;
    }

    public void setFechaDeContratacion(LocalDate fechaDeContratacion) {
        this.fechaDeContratacion = fechaDeContratacion;
    }

    @Override
    public String toString() {
        return "Profesor{" + super.toString()+ "tipoContrato=" + tipoContrato + ", fechaDeContratacion=" + fechaDeContratacion + '}';
    }


    
    
    
}
