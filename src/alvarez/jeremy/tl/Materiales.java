
package alvarez.jeremy.tl;

import java.time.LocalDate;
public class Materiales {
    private String asignatura;
    private LocalDate fechaComprqa;
    private char restringido;
    private String tema;

    public Materiales() {
    }

    public Materiales(String asignatura, LocalDate fechaComprqa, char restringido, String tema) {
        this.asignatura = asignatura;
        this.fechaComprqa = fechaComprqa;
        this.restringido = restringido;
        this.tema = tema;
    }

    public String getAsignatura() {
        return asignatura;
    }

    public void setAsignatura(String asignatura) {
        this.asignatura = asignatura;
    }

    public LocalDate getFechaComprqa() {
        return fechaComprqa;
    }

    public void setFechaComprqa(LocalDate fechaComprqa) {
        this.fechaComprqa = fechaComprqa;
    }

    public char getRestringido() {
        return restringido;
    }

    public void setRestringido(char restringido) {
        this.restringido = restringido;
    }

    public String getTema() {
        return tema;
    }

    public void setTema(String tema) {
        this.tema = tema;
    }

    @Override
    public String toString() {
        return "Materiales{" + "asignatura=" + asignatura + ", fechaComprqa=" + fechaComprqa + ", restringido=" + restringido + ", tema=" + tema + '}';
    }
    
    
}
