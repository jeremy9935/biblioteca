/*
atributos que el estudiante lleva 

fecah de creacion 17/6/2019
 */
package alvarez.jeremy.tl;

import java.time.LocalDate;
import alvarez.jeremy.tl.Profesor;

public class Estudiante extends Persona {

    private String carrera;
    private String numCreditos;

    public Estudiante() {
        super();
    }

    public Estudiante(String nombre, String apellido, String cedula,  String carrera, String nunCreditos) {
        super(nombre,apellido,cedula);
        this.carrera = carrera;
        this.numCreditos = numCreditos;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public String getNumCreditos() {
        return numCreditos;
    }

    public void setNumCreditos(String nunCreditos) {
        this.numCreditos = numCreditos;
    }

    @Override
    public String toString() {
        return "Estudiante{" + super.toString() + "carrera=" + carrera + ", numCreditos=" + numCreditos + '}';
    }

}
