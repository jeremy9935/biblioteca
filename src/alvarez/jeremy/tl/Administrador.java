/*
los datos que el administrador tiene 

fecha de creacion de la clase 17/6/2019
*/
package alvarez.jeremy.tl;

public class Administrador extends Persona {
    
    private char nombramiento;
    private String horasSemanales;

    public Administrador() {
        super();
    }
    
    public Administrador(String nombre, String apellido, String cedula, char nombramiento, String horasSemanales) {
        super(nombre,apellido,cedula);
        this.nombre = nombre;
        this.apellido = apellido;
        this.cedula = cedula;
        this.nombramiento = nombramiento;
        this.horasSemanales = horasSemanales;
    }


    public char getNombramiento() {
        return nombramiento;
    }

    public void setNombramiento(char nombramiento) {
        this.nombramiento = nombramiento;
    }

    public String getHorasSemanales() {
        return horasSemanales;
    }

    public void setHorasSemanales(String horasSemanales) {
        this.horasSemanales = horasSemanales;
    }

    @Override
    public String toString() {
        return "Administrador{"+super.toString() + "nombre=" + nombre + ", apellido=" + apellido + ", cedula=" + cedula + ", nombramiento=" + nombramiento + ", horasSemanales=" + horasSemanales + '}';
    }
    
    
    
    
}
